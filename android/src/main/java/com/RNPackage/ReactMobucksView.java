package com.RNPackage;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.widget.FrameLayout;
import com.facebook.react.bridge.ReactContext;
import com.google.android.gms.ads.MobileAds;
import com.mobucks.androidsdk.interfaces.AdListener;
import com.mobucks.androidsdk.views.BannerAdView;
import com.reactlibrary.R;

import java.util.Arrays;
import java.util.List;

public class ReactMobucksView extends FrameLayout {


    private ReactContext reactContext;
    private BannerAdView bannerAd;

    private String plid;
    private String password;
    private String uid;
    private float width;
    private  float height;

    private String googleAdUnit;

    private String webPropertyCode;
    private String Vlive_Smart;
    private String Home_Top;

    public ReactMobucksView(Context context) {
        super(context);
        reactContext = (ReactContext) context;
    }

    public ReactMobucksView(Context context, Activity activity) {
        super(context);
        reactContext = (ReactContext) context;

        FrameLayout counterLayout = (FrameLayout) activity.getLayoutInflater().inflate(R.layout.react_layout, null);
        this.addView(counterLayout);




        //lask-start
        bannerAd = (BannerAdView) counterLayout.findViewById(R.id.bannerView);

        bannerAd.setAdListener(new AdListener<BannerAdView>() {

            @Override
            public void onAdloaded(BannerAdView adView) {

                Log.d("MOBUCKS BANNER", "bannerAdView - onAdloaded");
            }

            @Override
            public void onAdFailed(Exception e) {

                Log.d("MOBUCKS BANNER", "bannerAdView - onAdFailed");



                e.printStackTrace();
            }

            @Override
            public void onAdClicked(BannerAdView adView) {
                Log.d("MOBUCKS BANNER", "bannerAdView - onAdClicked");
            }
        });

        //bannerAd.setAdSize(320,50);
        bannerAd.setVisibility(VISIBLE);
        //bannerAd.setBackgroundColor(Color.BLUE);

        //lask-end
    }

    public void loadBanner(){
        //this.bannerAd.setGoogleUnit("/302423036/SOX_App/Lock_screen");
       // this.bannerAd.setGoogleUnit("ca-app-pub-3940256099942544/6300978111");
        bannerAd.setAdSize((int)width,(int)height);
        bannerAd.loadAd();
    }

    public void setPlid(String placementId){
        this.plid = placementId;
        this.bannerAd.setPlacementId(this.plid);
    }

    public void setUid(String uid){
        this.uid = uid;
        this.bannerAd.setUid(this.uid);
    }

    public void setPassword(String password){
        this.password = password;
        this.bannerAd.setPassword(this.password);
    }

    public void setWebPropertyCode(String webPropertyCode) {
        this.webPropertyCode = webPropertyCode;
        this.bannerAd.setWebPropertyCode(this.webPropertyCode);
    }

    public void setVlive_Smart(String vlive_Smart) {
        this.Vlive_Smart = vlive_Smart;
        this.bannerAd.setVlive_Smart(this.Vlive_Smart);
    }

    public void setHome_Top(String home_Top) {
        this.Home_Top = home_Top;
        this.bannerAd.setHome_Top(this.Home_Top);
    }

    public void setGoogleUnit(String googleAdUnit){
        this.googleAdUnit = googleAdUnit;
        this.bannerAd.setGoogleUnit(this.googleAdUnit);
    }

    public void setWidth(float width){
        this.width = width;
    }

    public void setHeight(float height){
        this.height = height;
    }

    @Override
    public void requestLayout() {
        super.requestLayout();
        post(measureAndLayout);
    }

    private final Runnable measureAndLayout = new Runnable() {
        @Override
        public void run() {
            measure(
                    MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.EXACTLY));
            layout(getLeft(), getTop(), getRight(), getBottom());
        }
    };

}

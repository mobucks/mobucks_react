package com.RNPackage;



import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;


import javax.annotation.Nullable;
import java.util.Map;


public class ReactMobucksViewManager extends SimpleViewManager<ReactMobucksView> {

    private static final String REACT_CLASS = "RCTMobucksView";
    public static final int COMMAND_LOAD_BANNER = 1;
    public static final int COMMAND_SET_PROPS = 0;


    @Override
    public String getName() {
        return REACT_CLASS;
    }

    //Comment this method and un-comment the method under.
    /*@Override
    protected ReactMobucksView createViewInstance(ThemedReactContext reactContext) {
        return new ReactMobucksView(reactContext);
    }*/


     //Comment out this if you want to run it on a ReactNative app and Comment the above one
     @Override
     protected ReactMobucksView createViewInstance(ThemedReactContext reactContext) {
        return new ReactMobucksView(reactContext, reactContext.getCurrentActivity());
     }

    @ReactProp(name = "plid")
    public void setPlacementId(ReactMobucksView view, String plid) {

        view.setPlid(plid);
    }

    @ReactProp(name = "uid")
    public void seUId(ReactMobucksView view, String uid) {

        view.setUid(uid);
    }
    @ReactProp(name = "password")
    public void setPassword(ReactMobucksView view, String pass) {

        view.setPassword(pass);
    }
    @ReactProp(name = "width")
    public void setWidth(ReactMobucksView view, float width ) {

        view.setWidth(width);
    }

    @ReactProp(name = "height")
    public void setHeight(ReactMobucksView view, float height) {

        view.setHeight(height);

    }

    @ReactProp(name = "Vlive_Smart")
    public void setVliveSmart(ReactMobucksView view, String Vlive_Smart) {

        view.setVlive_Smart(Vlive_Smart);
    }

    @ReactProp(name = "Home_Top")
    public void setHometop(ReactMobucksView view, String Home_Top) {

        view.setHome_Top(Home_Top);
    }

    @ReactProp(name = "webPropertyCode")
    public void setwebPropCode(ReactMobucksView view, String webPropertyCode) {

        view.setWebPropertyCode(webPropertyCode);
    }

     @ReactProp(name = "googleAdUnit")
        public void setGoogleUnitId(ReactMobucksView view, String googleAdUnit) {

            view.setGoogleUnit(googleAdUnit);
        }

    @ReactProp(name = "startLoading")
        public void setLoading(ReactMobucksView view, String startLoading) {

            if(startLoading.equals("true")){
                view.loadBanner();
            }

        }

    /*
    *   ---------------   REACT METHODS  -------------------
    * */

    @ReactMethod
    public void setPlid(String plid, ReactMobucksView view) {
        view.setPlid(plid);
    }

    @Nullable
    @Override
    public Map<String, Integer> getCommandsMap() {
        return MapBuilder.of("loadBanner", COMMAND_LOAD_BANNER);
    }

    @Override
    public void receiveCommand(ReactMobucksView root, int commandId, @javax.annotation.Nullable ReadableArray args) {
        switch (commandId) {
            case COMMAND_LOAD_BANNER:
                root.loadBanner();
                break;

            case COMMAND_SET_PROPS:
                root.loadBanner();
                break;
        }
    }

}

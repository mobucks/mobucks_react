
# react-native-mobucks-library

## Getting started

Run these two commands inside your project:

`$ npm install https://mobucks@bitbucket.org/mobucks/mobucks_react.git --save`

`$ react-native link react-native-mobucks-library`

You can check if it is installed correctly inside the package.json file.

### Manual installation


#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.RNPackage.ReactMobucksPackage;` to the imports at the top of the file
  
  - Add `packages.add(new ReactMobucksPackage());` to the list returned by the `getPackages()` method
  
  
``
            @Override
            protected List<ReactPackage> getPackages() {
               List<ReactPackage> packages = new PackageList(this).getPackages();
              // Packages that cannot be autolinked yet can be added manually here, for example:
              packages.add(new ReactMobucksPackage());
              return packages;
        }
``
  
##### The above are set automatically, if they have not - do it manually as follows:
  
2. Append the following lines to `android/settings.gradle`:
  	
  	`include ':react-native-mobucks-library'`
  	`project(':react-native-mobucks-library').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-mobucks-library/android')`
  	
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	
      `compile project(':react-native-mobucks-library')`
  	


## Usage

1. Create a .js file inside your project and name it whatever you want e.g MobucksSDK.js.

2. Open that file and add the following code:

``
    import React, {Component} from 'react';
    import {findNodeHandle, requireNativeComponent, UIManager} from 'react-native';
    
    class AdMobucksBanner extends Component {
      constructor() {
        super();
        this.state = {
          style: {},
        };
      }
    
      render() {
        return (
          <BannerAdView
            {...this.props}
            style={[this.props.style, this.state.style]}
            ref={el => (this._bannerView = el)}
          />
        );
      }
      loadBanner() {
        UIManager.dispatchViewManagerCommand(
          findNodeHandle(this._bannerView),
          UIManager.getViewManagerConfig('RCTMobucksView').Commands.loadBanner,
          null,
        );
      }
    }
    
    const BannerAdView = requireNativeComponent('RCTMobucksView', AdMobucksBanner);
    
    export default AdMobucksBanner;
``

Now you can use AdMobucksBanner Component to show a banner.

#### Use AdMobucksBanner Component 

1. Import the Component from the js file you create above (MobucksSDK.js).

`import AdMobucksBanner from '.(path to directory)/MobucksSDK'; ` 
   

2. Add the component:

```
  <AdMobucksBanner
        uid="vodacomsa"
        plid="875"
        password="bd3156**********"
        width="320"
        height="150"
       googleAdUnit="--Your adUnit--"
        startLoading="true"

        style={{
          height: 400,
          width: 420,
        }}
      />
```

  Properties:
  
  Basic banner properties:
  
    - uid="vodacomsa"
    
    - plid="875"
    
    - password="bd3156**********"
   
   Size of the requested banner: --> !! This is SDKs property it doesnt change the view of the component !! 
   
    -  width="320"
    
    -  height="150"
   
   Google properties(optional: If you want to show google banner)
   
    -  Vlive_Smart="2166***"
    
    -  Home_Top="2170****"
    
    -  webPropertyCode="ca-mb-app-pub-*********"
    
    -   googleAdUnit="..."<---- Try this with google
   
   If you want to load the banner directly with the load of the page.
   
    -  startLoading="true"
   
   You have to add component styles:
   
    - `` style={{
                 height: 400,
                 width: 420,
               }}``
   this width and height are the properties which control the view of the banner on the screen.
   
3. Component has a function for loading the banner. You can use it as follows:

```js
    <Button
          title="Reload"
          onPress={() => this._basicExample.loadBanner()}
        />
```

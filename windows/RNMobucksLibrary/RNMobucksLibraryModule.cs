using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Mobucks.Library.RNMobucksLibrary
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNMobucksLibraryModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNMobucksLibraryModule"/>.
        /// </summary>
        internal RNMobucksLibraryModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNMobucksLibrary";
            }
        }
    }
}
